// 1. Create a class function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`
Class Spaceship {
	constructor(name, topSpeed) {
		this.name = name;
		this.topSpeed = topSpeed;
	}
	acceltopSpeed() {
		const { name, topSpeed } = this;
		console.log (`${name} accel ${topSpeed}`};
	};
};


// 2. Call the constructor with a couple ships, 
// and call accelerate on both of them.

const challenger = new Spaceship('Challenger', 50000);
challenger.acceltopSpeed();

