// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price

const logReceipt = function(...items)
{
  let total = 0;
  
  items.forEach { x => {
    console.log('  ${x.descr} - \$${x.price}' );
    totl += x.price;
  }}
  console.log( 'total - \$${total}');
}:



const bud = { descr: 'Bud Light', price: 3.99 };
const burger = { descr: 'Hamburger', price: 6.99 };
logReceipt(bud, burger);



// Check
logReceipt(
  { descr: 'Burrito', price: 5.99 },
  { descr: 'Chips & Salsa', price: 2.99 },
  { descr: 'Sprite', price: 1.99 }
);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97

const sumNumbers = function () {
let sum = 0;
const args = Array.from(arguments);
args.forEach(function (number) {
sum += number;
});
return sum;